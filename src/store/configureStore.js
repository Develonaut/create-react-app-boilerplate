import { createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web and AsyncStorage for react-native

import rootReducer from './reducers';

const persistConfig = {
  key: 'root',
  storage,
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

const REDUX_DEVTOOLS = (process.env.NODE_ENV !== 'production')
  ? window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  : null;

export default () => {
  let store = createStore(persistedReducer, REDUX_DEVTOOLS);
  let persistor = persistStore(store)
  return { store, persistor }
}