import { createSelector } from 'reselect'


// Selectors
const exampleSelector = state => state.example;

export const getExampleString = createSelector(
  exampleSelector,
  example => example,
);

// Actions
export const EXAMPLE_ACTION = 'EXAMPLE_ACTION';

export function exampleAction({ exampleData = 'example'}) {
  return { type: EXAMPLE_ACTION, exampleData }
}

const initState = {
  "example": "testExample",
};

export default function UIModule(state = initState, action) {
    switch (action.type) {
      case EXAMPLE_ACTION:
        return {
          ...state,
          "example": action.exampleData,
        }
      default:
        return state
    }
  }